Feature: Serach Product on Ecommerce site

Background: Launch HomePage
 Given User is on Home Page
 Then Home Page must be displayed 
 
@SearchTest
Scenario Outline: To verify product serach results
    When user enter search query "<serach_query>"
    And user click on Search Icon
    Then product "<product_name>" is displayed on serach result page
    
    Examples: 
    | serach_query | product_name |
    | Faded Short | Faded Short Sleeve T-shirts |
    | Chiffon Dress | Printed Chiffon Dress |
    