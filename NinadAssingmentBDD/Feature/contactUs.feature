Feature: Contact Us Message on Ecommerce site

Background: Launch HomePage
 Given User is on Home Page
 Then Home Page must be displayed 

  @ContactTest
  Scenario: To verify Contact Us Message
    When user is on Contact Us Page
    And user enter the following messages details
      | Subject Heading  | Email address | Order reference | Message      |
      | Customer service | test@xyz.com  | ABCDEFG         | Test Message |
    And click on Send Button
    Then message successfully sent is displayed
