Feature: Add Product to Cart on Ecommerce site

Background: Launch HomePage
 Given User is on Home Page
 Then Home Page must be displayed 
 
@CartTest
  Scenario: To verify complete order process
    When user click on "Womens" Category
    And user click on "Blouse" Product
    And user update product quantity to "2"
    And user selects the product size to "M"
    And user selects the product color to "Black"
    And user Click on Add To Cart Button
    Then Proceed to checkout Button must be displayed