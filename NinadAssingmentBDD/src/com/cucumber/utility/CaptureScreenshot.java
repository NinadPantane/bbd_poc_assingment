package com.cucumber.utility;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class CaptureScreenshot {

	public void screenCapture() throws Exception {

		TestBase baseTestPage = new TestBase();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");
		LocalDateTime now = LocalDateTime.now();
		
		TakesScreenshot scrShot = ((TakesScreenshot)baseTestPage.getDriver());
		File srcFile = scrShot.getScreenshotAs(OutputType.FILE);

		// Move image file to new destination
		File destFile = new File(System.getProperty("user.dir")+"/screenshots/"+ dtf.format(now)+".jpg");

		// Copy file at destination
		FileUtils.copyFile(srcFile, destFile);
		

	}
}
