package com.cucumber.utility;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.junit.AfterClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import cucumber.api.Scenario;

public class TestBase {

	static WebDriver driver;
	String browserName = ReadingProperties.getBrowserName();
	String url = ReadingProperties.getAppUrl();
	long wait = Long.parseLong(ReadingProperties.getImplicitWait());

	//To Open Browser
	public void getBrowser() {
		if(browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(wait, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			Set<String> winHandles = driver.getWindowHandles();
			int numberOfWindows = winHandles.size();
			if (numberOfWindows == 1) {
				System.out.println("Browser is Open..");
			} else {
				System.out.println("Browser is Closed");
			}
		}

		else if(browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
			FirefoxOptions options = new FirefoxOptions();
			options.setCapability("marionette", true);
			driver = new FirefoxDriver(options);
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(wait, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			Set<String> winHandles = driver.getWindowHandles();
			int numberOfWindows = winHandles.size();
			if (numberOfWindows == 1) {
				System.out.println("Browser is Open..");
			} else {
				System.out.println("Browser is Closed");
			}
		}
		else if (!(browserName.equalsIgnoreCase("chrome") || browserName.equalsIgnoreCase("firefox"))) {
			System.out.println("Browser is not installed in the system");
		}
	}

	public void getUrl() {
		driver.navigate().to(url);
	}

	public WebDriver getDriver() {
		if (driver == null) {
			if(browserName.equalsIgnoreCase("chrome"))
			{driver = new ChromeDriver();}
			else if(browserName.equalsIgnoreCase("firefox"))
			{{driver = new FirefoxDriver();}}
			return driver;
		} else {
			return driver;
		}
	}

	@AfterClass
	public void tearDown(Scenario scenario) throws Exception {
		if (scenario.isFailed()) {
			// Take a screenshot...
			CaptureScreenshot takescreenshot = new CaptureScreenshot();
			takescreenshot.screenCapture();
			getDriver().quit();
		}
	}

	public void closeBrowser() {
		getDriver().quit();
	}
}
