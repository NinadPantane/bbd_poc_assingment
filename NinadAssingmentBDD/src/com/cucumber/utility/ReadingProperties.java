package com.cucumber.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ReadingProperties {

	public static Properties getProp() {
	Properties prop = new Properties();
	
	try {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "/resources/application.properties");
		try {
			prop.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	return prop;
	}
	
	public static String getBrowserName() {
		String browserName = getProp().getProperty("browserName");
		if (browserName != null) return browserName;
		else throw new RuntimeException("Browser Name is not provided in application.properties file for the key: browserName");
	}
	
	public static String getImplicitWait() {
		String impicitWait = getProp().getProperty("impicitWait");
		if (impicitWait != null) return impicitWait;
		else throw new RuntimeException("ImpicitWait is not provided in application.properties file for the key: impicitWait");
	}
	
	public static String getAppUrl() {
		String url = getProp().getProperty("url");
		if (url != null) return url;
		else throw new RuntimeException("App url is not provided in application.properties file for the key: url");
	}
}