package com.cucumber.TestRunner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
@RunWith(Cucumber.class)
@CucumberOptions( plugin = {"pretty","html:target/cucumber-html-reports/html","json:target/cucumber-jason-reports/Cucumber.json"},
				//tags = {"~@ContactTest","@SearchTest","~@CartTest"},
                features = {"Feature"},
                glue={"com.cucumber.steps"},
                monochrome = true,
                dryRun = false
        )
public class TestRunner {

}
