package com.cucumber.steps;

import static org.junit.Assert.assertTrue;
import com.cucmber.pages.HomePage;
import com.cucumber.utility.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class backgroundStep extends TestBase{

	TestBase baseTestPage;
	HomePage homePage;


	@Given("User is on Home Page")
	public void user_is_on_home_page() {
		homePage = new HomePage(getDriver());
		homePage.waitForPageToLoad();
	}


	@Then("Home Page must be displayed")
	public void home_Page_must_be_displayed() {
		assertTrue(homePage.homePageSlider.isDisplayed());
		System.out.println("Background Completed");
	}
}
