package com.cucumber.steps;

import static org.junit.Assert.assertTrue;

import java.util.List;

import com.cucmber.pages.HomePage;
import com.cucmber.pages.SearchPage;
import com.cucumber.utility.TestBase;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class searchProductStep extends TestBase{

	HomePage homePage;
	SearchPage searchPage;
	TestBase baseTestPage = new TestBase();
	
	@When("^user enter search query \"([^\"]*)\"$")
	public void user_enter_search_query(String search_query) {
		homePage = new HomePage(getDriver());
		homePage.textboxSearch.sendKeys(search_query);
	}

	@And("user click on Search Icon")
	public void user_click_on_Search_Icon(){
		homePage.buttonSearch.click();
	}

	@Then("^product \"([^\"]*)\" is displayed on serach result page$")
	public void product_is_displayed_on_serach_result_page(String product_name) {
		searchPage = new SearchPage(getDriver());
		searchPage.waitForPageToLoad();
		List<String> searchResult = searchPage.getSearchResultProductList();
		assertTrue(searchResult.contains(product_name));
	}

	

}
