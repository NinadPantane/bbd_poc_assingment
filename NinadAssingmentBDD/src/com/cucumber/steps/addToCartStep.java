package com.cucumber.steps;

import static org.junit.Assert.assertTrue;

import com.cucmber.pages.CategoryWomensPage;
import com.cucmber.pages.HomePage;
import com.cucmber.pages.ProductBlousePage;
import com.cucumber.utility.TestBase;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class addToCartStep extends TestBase{

	TestBase baseTestPage = new TestBase();;
	HomePage homePage;
	CategoryWomensPage womensPage;
	ProductBlousePage blousePage;

	@When("^user click on \"([^\"]*)\" Category$")
	public void user_click_on_Category(String category_name)throws Throwable {
		homePage = new HomePage(getDriver());
		homePage.waitForPageToLoad();
		if(category_name.equals("Womens"))
		{homePage.categoryWomen.click();}
		else 
		{
			throw new Exception("Category not found");
		}
	}

	@And("^user click on \"([^\"]*)\" Product$")
	public void user_click_on_Product(String product_name) throws Throwable {
		womensPage = new CategoryWomensPage(getDriver());
		womensPage.waitForPageToLoad();
		womensPage.selectProduct(product_name);
	}

	@And("^user update product quantity to \"([^\"]*)\"$")
	public void user_update_product_quantity_to(String product_qty) {
		blousePage = new ProductBlousePage(getDriver());
		blousePage.waitForPageToLoad();
		blousePage.inputQuantity.clear();
		blousePage.inputQuantity.sendKeys(product_qty);
	}

	@And("^user selects the product size to \"([^\"]*)\"$")
	public void user_selects_the_product_size_to(String product_size) {
		blousePage.selectSize(product_size);
	}

	@And("^user selects the product color to \"([^\"]*)\"$")
	public void user_selects_the_product_color_to(String product_color) {
		blousePage.selectColor(product_color);
	}

	@And("user Click on Add To Cart Button")
	public void user_Click_on_Button() {
		blousePage.buttonAddToCart.click();
	}

	@Then("Proceed to checkout Button must be displayed")
	public void proceed_to_checkout_button_displayed(){
		blousePage.waitForPopUpToLoad();
		assertTrue(blousePage.buttonProceedToCheckout.isDisplayed());
	}
	


}
