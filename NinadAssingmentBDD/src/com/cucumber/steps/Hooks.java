package com.cucumber.steps;


import com.cucumber.utility.TestBase;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends TestBase {

	@Before
	public void beforeHook() {
		System.out.println("This will run before the Scenario");
		getBrowser();
		getUrl();
	}
	
	@After
	public void afterHook() {
		System.out.println("This will run after the Scenario");
		closeBrowser();
	}
}
