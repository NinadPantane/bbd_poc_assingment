package com.cucumber.steps;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;
import com.cucmber.pages.ContactUsPage;
import com.cucmber.pages.HomePage;
import com.cucumber.utility.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class contactUsSteps extends TestBase{

	TestBase baseTestPage = new TestBase();;
	HomePage homePage;
	ContactUsPage contactUsPage;

	@When("user is on Contact Us Page")
	public void user_is_on_ContactUs_Page() {
		homePage = new HomePage(getDriver());
		homePage.waitForPageToLoad();
		homePage.linkContactUs.click();
		
		contactUsPage = new ContactUsPage(getDriver());
		contactUsPage.waitForPageToLoad();
	}

	@And("^user enter the following messages details$")
	public void user_contactUs_details(DataTable table) {
		table.raw();
		  List<Map<String, String>> list = table.asMaps(String.class, String.class);
		  contactUsPage.selectSubjectHeading(list.get(0).get("Subject Heading"));
		  contactUsPage.textboxEmailAddress.sendKeys(list.get(0).get("Email address"));
		  contactUsPage.textboxOrderReference.sendKeys(list.get(0).get("Order reference"));
		  contactUsPage.textAreaMessage.sendKeys(list.get(0).get("Message"));
		 
	}

	@And("click on Send Button")
	public void clic_Send_Button() {
		contactUsPage.buttonSend.click();
	}

	@Then("message successfully sent is displayed")
	public void send_success_message_displayed(){
		assertTrue(contactUsPage.textMessageSuccessfullySent.getText().trim().contentEquals("Your message has been successfully sent to our team."));
	}
	
}
