package com.cucmber.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cucumber.utility.BasePage;


public class SearchPage extends BasePage{

	public SearchPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//h1[@class='page-heading  product-listing']")
	public WebElement searchHeader;

	@FindBy(xpath = "//a[@class='login']")
	public WebElement linkSignIn;

	@FindBy(xpath = "//input[@id='search_query_top']")
	public WebElement textboxSearch;

	@FindBy(xpath = "//input[@id='search_query_top']/following::button[1]")
	public WebElement buttonSearch;

	@FindBy(xpath = "//ul[@class='product_list grid row']/li//h5/a")
	public List<WebElement> searchProductResult;

	public void waitForPageToLoad() {
		try {
			Thread.sleep(1200);
			searchHeader.isDisplayed();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public List<String> getSearchResultProductList() {
		List<WebElement> productResult = searchProductResult;
		List<String> productList = new ArrayList<String>();
		for (WebElement webElement : productResult) {
			productList.add(webElement.getText());
		}
		return productList;
	}



}
