package com.cucmber.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.cucumber.utility.BasePage;


public class ContactUsPage  extends BasePage {

	public ContactUsPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//h1[@class='page-heading bottom-indent']")
	public WebElement headerContactUs;
	
	@FindBy(xpath = "//input[@id='email']")
	public WebElement textboxEmailAddress;
	
	@FindBy(xpath = "//input[@id='id_order']")
	public WebElement textboxOrderReference;
	
	@FindBy(xpath = "//textarea[@id='message']")
	public WebElement textAreaMessage;
	
	@FindBy(xpath = "//button[@id='submitMessage']")
	public WebElement buttonSend;
	
	@FindBy(xpath = "//p[@class='alert alert-success']")
	public WebElement textMessageSuccessfullySent;
	
	 public void selectSubjectHeading(String heading){
		   Select productSize = new Select(driver.findElement(By.xpath("//select[@id='id_contact']")));
		   productSize.selectByVisibleText(heading);
	   }

	   public void waitForPageToLoad() {
			try {
				Thread.sleep(1200);
				headerContactUs.isDisplayed();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
}
