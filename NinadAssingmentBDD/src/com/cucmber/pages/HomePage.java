package com.cucmber.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cucumber.utility.BasePage;


public class HomePage extends BasePage{

	   public HomePage(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//div[@id='homepage-slider']")
	   public WebElement homePageSlider;

	   @FindBy(xpath = "//a[@class='login']")
	   public WebElement linkSignIn;
	   
	   @FindBy(xpath = "//a[@title='Contact Us']")
	   public WebElement linkContactUs;
	   
	   @FindBy(xpath = "//input[@id='search_query_top']")
	   public WebElement textboxSearch;
	   
	   @FindBy(xpath = "//input[@id='search_query_top']/following::button[1]")
	   public WebElement buttonSearch;
	   
	   @FindBy(xpath = "//li/a[text()='Women']")
	   public WebElement categoryWomen;

	   
		/*
		 * WebDriver driver; HomePage page = PageFactory.initElements(driver,
		 * HomePage.class);
		 */
	   
		/*
		 * //Constructor public HomePage(WebDriver driver) { this.driver = driver;
		 * PageFactory.initElements(driver, this); }
		 */
	   public void waitForPageToLoad() {
			try {
				Thread.sleep(1200);
				homePageSlider.isDisplayed();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	   
}
