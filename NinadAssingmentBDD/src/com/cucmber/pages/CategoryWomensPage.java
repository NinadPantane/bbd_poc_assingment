package com.cucmber.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.cucumber.utility.BasePage;


public class CategoryWomensPage extends BasePage{

	   public CategoryWomensPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//span[@class='cat-name'][contains(text(),'Women')]")
	   public WebElement categoryWomenHeader;

	   @FindBy(xpath = "//a[@class='login']")
	   public WebElement linkSignIn;
	   
	   @FindBy(xpath = "//input[@id='search_query_top']")
	   public WebElement textboxSearch;
	   
	   @FindBy(xpath = "//input[@id='search_query_top']/following::button[1]")
	   public WebElement buttonSearch;
	   
	   @FindBy(xpath = "//li/a[text()='Women']")
	   public WebElement categoryWomen;

	   public void waitForPageToLoad() {
			try {
				Thread.sleep(1200);
				categoryWomenHeader.isDisplayed();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	   
	   public void selectProduct(String productName) {
		   WebElement productname = driver.findElement(By.xpath("//ul[@class='product_list grid row']//div[@class='product-container']//h5/a[contains(text(),'"+productName+"')]"));
		   productname.click();
	   }
	   
}
