package com.cucmber.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cucumber.utility.BasePage;


public class SignInPage  extends BasePage {

	public SignInPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//h1[text()='Authentication']")
	public WebElement headerAuthentication;

	@FindBy(xpath = "//h3[text()='Create an account']")
	public WebElement textCreateAnAccount;
	
	@FindBy(xpath = "//h3[text()='Create an account']//following::input[@id='email_create']")
	public WebElement textboxCreateAcctEmailAddress;
	
	@FindBy(xpath = "//button[@name='SubmitCreate']")
	public WebElement buttonCreateAccount;
	
	@FindBy(xpath = "//h3[text()='Already registered?']")
	public WebElement textAlreadyRegistered;
	
	@FindBy(xpath = "//h3[text()='Already registered?']//following::label[text()='Email address']")
	public WebElement labelEmailAddress;
	
	@FindBy(xpath = "//h3[text()='Already registered?']//following::input[@id='email']")
	public WebElement textboxEmailAddress;
	
	@FindBy(xpath = "//h3[text()='Already registered?']//following::label[text()='Password']")
	public WebElement labelPassword;
	
	@FindBy(xpath = "//h3[text()='Already registered?']//following::input[@id='passwd']")
	public WebElement textboxPassword;
	
	@FindBy(xpath = "//button[@id='SubmitLogin']")
	public WebElement buttonSignIn;
	
	   public void waitForPageToLoad() {
			try {
				Thread.sleep(1200);
				headerAuthentication.isDisplayed();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
}
