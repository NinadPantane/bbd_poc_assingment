package com.cucmber.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.cucumber.utility.BasePage;


public class ProductBlousePage extends BasePage{

	   public ProductBlousePage(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//h1[text()='Blouse']")
	   public WebElement productBlouseHeader;

	   @FindBy(xpath = "//a[@class='login']")
	   public WebElement linkSignIn;
	   
	   @FindBy(xpath = "//input[@id='search_query_top']")
	   public WebElement textboxSearch;
	   
	   @FindBy(xpath = "//input[@id='search_query_top']/following::button[1]")
	   public WebElement buttonSearch;
	   
	   @FindBy(xpath = "//li/a[text()='Women']")
	   public WebElement categoryWomen;
	   
	   @FindBy(xpath = "//input[@id='quantity_wanted']")
	   public WebElement inputQuantity;
	   
	   @FindBy(xpath = "//button[span[text()='Add to cart']]")
	   public WebElement buttonAddToCart;
	   
	   @FindBy(xpath = "//div[@id='layer_cart']")
	   public WebElement popUpCart;
	   
	   @FindBy(xpath = "//a[@title='Proceed to checkout']")
	   public WebElement buttonProceedToCheckout;

	   public void waitForPageToLoad() {
			try {
				Thread.sleep(1200);
				productBlouseHeader.isDisplayed();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	   
	   public void waitForPopUpToLoad() {
			try {
				Thread.sleep(1200);
				popUpCart.isDisplayed();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	   
	   public void selectSize(String size){
		   Select productSize = new Select(driver.findElement(By.xpath("//select[@id='group_1']")));
		   productSize.selectByVisibleText(size);
	   }
	 
	   public void selectColor(String color){
		   WebElement productColor = driver.findElement(By.xpath("//ul[@id='color_to_pick_list']//li/a[@title='"+color+"']"));
		   productColor.click();
	   }
	   
}
